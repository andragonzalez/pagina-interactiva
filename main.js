$(document).ready(function(){

  // Comprobar el estado inicial
  if($(".ubicaciones").prop('checked')) {
    $('#Ubicaciones').show();
  } else {
    $('#Ubicaciones').hide();
  }

  if($(".mejoras").prop('checked')) {
    $('#Mejoras').show();
  } else {
    $('#Mejoras').hide();
  }

  if($(".camiones").prop('checked')) {
    $('#Camiones').show();
  } else {
    $('#Camiones').hide();
  }


  // Cuando cambie nombres
$('.ubicaciones').change(function(){
  if(!$(this).prop('checked')){
    $('#Ubicaciones').hide();
    }else{
    $('#Ubicaciones').show();
    }
  });

// Cuando cambie armas
$('.mejoras').change(function(){
  if(!$(this).prop('checked')){
    $('#Mejoras').hide();
    }else{
    $('#Mejoras').show();
    }
  });

// Cuando cambie camiones
$('.camiones').change(function(){
  if(!$(this).prop('checked')){
    $('#Camiones').hide();
    }else{
    $('#Camiones').show();
    }
});

// Mostrar foto del camión en el click:
$(".camion").on("click", function() {
  // Cerrar todos los popups
  $(".popup").hide();
  // Abrir este popup
  $(this).find(".popup").toggle();
});

$(".mejora").on("click", function() {
  // Cerrar todos los popups
  $(".popup").hide();
  // Abrir este popup
  $(this).find(".popup").toggle();
});




});
